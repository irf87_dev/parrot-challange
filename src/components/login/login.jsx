import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classNames from 'classnames';
import actions from '../../rdx/actions/auth-actions';
import { LOGIN_FIELDS } from '../../constants/fields';
import validator from '../../utils/validators';
import Error from '../share/error';

class Login extends Component {
    constructor() {
        super();
        this.state = {
            username: {
                text: '',
                isValid: false,
            },
            password: {
                text: '',
                isValid: false,
            },
        };
    }

    componentDidMount() {
        const { access, onValidateToken, history } = this.props;
        if (access) onValidateToken(history);
    }

    onSetValue(fieldName, value) {
        const { [fieldName]: oField } = this.state;
        let isValid = false;
        switch (fieldName) {
            case LOGIN_FIELDS.EMAIL.fieldName:
                isValid = value && validator.emailVal(value);
                break;
            case LOGIN_FIELDS.PASSWORD.fieldName:
                isValid = (value);
                break;
            default:
        }
        oField.isValid = isValid;
        oField.text = value;
        this.setState({ [fieldName]: oField });
    }

    render() {
        const { onLogin, history } = this.props;
        const { username, password } = this.state;
        const isValidForm = username.isValid && password.isValid;
        const btnClass = classNames({
            'waves-effect': true,
            'waves-light': true,
            'btn-large': true,
            disabled: !isValidForm,
        });
        return (
            <div className="login-screen">
                <div className="row">
                    <div className="login-container col s12 m6 l4">
                        <h4>Bienvenido</h4>
                        <div className="input-field col s12">
                            <input
                                placeholder="Usuario"
                                id="input_email"
                                type="email"
                                className={(username.isValid) ? 'validate' : ''}
                                value={username.text}
                                onChange={(e) => {
                                    if (e.keyCode === 13) return;
                                    this.onSetValue(LOGIN_FIELDS.EMAIL.fieldName, e.target.value);
                                }}
                            />
                        </div>
                        <div className="input-field col s12">
                            <input
                                placeholder="Contraseña"
                                id="first_name"
                                type="password"
                                className={(password.isValid) ? 'validate' : ''}
                                value={password.text}
                                onChange={(e) => {
                                    if (e.keyCode === 13) return;
                                    this.onSetValue(LOGIN_FIELDS.PASSWORD.fieldName,
                                        e.target.value);
                                }}
                            />
                            <div>
                                <Error />
                            </div>
                        </div>
                        <div className="input-field col s12">
                            <button
                                className={btnClass}
                                type="button"
                                onClick={() => {
                                    onLogin(username.text, password.text, history);
                                }}
                            >
                                Login
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    access: PropTypes.string,
    onLogin: PropTypes.func,
    onValidateToken: PropTypes.func,
    history: PropTypes.objectOf(PropTypes.any),
};

const mapStateToProps = state => ({
    access: state.user.access,
});

const mapDispatchToProps = dispatch => ({
    onLogin: (username, password, history) => dispatch(
        actions.doLogin(username, password, history),
    ),
    onValidateToken: history => dispatch(actions.validateToken(history)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
