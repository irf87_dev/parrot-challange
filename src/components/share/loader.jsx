import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const Loader = ({ showed, msg }) => (
    (showed) ? (
        <div className="loader-content">
            <div className="preloader-wrapper big active">
                <div className="spinner-layer spinner-blue-only">
                    <div className="circle-clipper left">
                        <div className="circle" />
                    </div>
                    <div className="gap-patch">
                        <div className="circle" />
                        <div className="circle-clipper right" />
                        <div className="circle" />
                    </div>
                </div>
            </div>
            {
                (msg) ? (
                    <section className="loader-msg">
                        <p>
                            { msg }
                        </p>
                    </section>
                ) : null
            }
        </div>
    ) : <React.Fragment />
);

Loader.propTypes = {
    showed: PropTypes.bool,
    msg: PropTypes.string,
};

const mapStateToProps = state => ({
    showed: state.ui.loader.showed,
    msg: state.ui.loader.msg,
});

export default connect(mapStateToProps, {})(Loader);
