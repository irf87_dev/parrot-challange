import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const Error = ({ showed, msg }) => (
    (showed) ? (
        <p className="error-general">
            {msg}
        </p>
    ) : <React.Fragment />
);

Error.propTypes = {
    showed: PropTypes.bool,
    msg: PropTypes.string,
};

const mapStateToProps = state => ({
    showed: state.ui.globalErrors.showed,
    msg: state.ui.globalErrors.msg,
});

export default connect(mapStateToProps, {})(Error);
