import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import actions from '../../rdx/actions/menu-actions';
import Product from './product';

const Categories = ({ categories, products, onUpdate }) => (
    <ul className="collapsible" id="collapsible-id">
        {
            (categories.length) ? (
                categories.map(category => (
                    <li key={category.uuid}>
                        <div className="collapsible-header">
                            { category.name}
                            <span className="badge">
                                { category.countingProducts}
                            </span>
                        </div>
                        <div className="collapsible-body collapsible-product">
                            {
                                products.map(product => (
                                    (product.category.uuid
                                        === category.uuid) ? (
                                            <Product
                                                key={product.uuid}
                                                product={product}
                                                onUpdate={onUpdate}
                                            />
                                        ) : null
                                ))
                            }
                        </div>
                    </li>
                ))
            ) : <li />
        }
    </ul>
);

Categories.propTypes = {
    categories: PropTypes.arrayOf(PropTypes.object),
    products: PropTypes.arrayOf(PropTypes.object),
    onUpdate: PropTypes.func,
};

const mapStateToProps = state => ({
    currentStore: state.menu.currentStore,
    stores: state.menu.stores,
});

const mapDispatchToProps = dispatch => ({
    onUpdate: (productId, isAvailable) => dispatch(actions.updataAvailable(productId, isAvailable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
