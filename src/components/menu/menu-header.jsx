import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import actions from '../../rdx/actions/menu-actions';

const NavHeader = ({ currentStore, stores, onSelectStore }) => (
    <div>
        <ul id="dropdown1" className="dropdown-content">
            {
                (stores.length > 0) ? (
                    stores.map(store => (
                        <li key={store.uuid}>
                            <a onClick={() => onSelectStore(store)}>
                                {store.name}
                            </a>
                        </li>
                    ))
                ) : <li />
            }
        </ul>
        <div className="navbar-fixed">
            <nav>
                <div className="nav-wrapper">
                    <ul className="left" id="main-list">
                        <li>
                            <a className="dropdown-trigger" data-target="dropdown1">
                                { currentStore.name }
                                <i className="material-icons right">{ '>' }</i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
);

NavHeader.propTypes = {
    currentStore: PropTypes.objectOf(PropTypes.any),
    stores: PropTypes.arrayOf(PropTypes.object),
    onSelectStore: PropTypes.func,
};

const mapStateToProps = state => ({
    currentStore: state.menu.currentStore,
    stores: state.menu.stores,
});

const mapDispatchToProps = dispatch => ({
    onSelectStore: store => dispatch(actions.setCurrentStore(store)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NavHeader);
