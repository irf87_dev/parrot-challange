import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import actions from '../../rdx/actions/menu-actions';
import authActions from '../../rdx/actions/auth-actions';
import NavHeader from './menu-header';
import Categories from './categories';
import M from '../../../node_modules/materialize-css/dist/js/materialize';
import utils from '../../utils/categories';

const TIME = (25 * 60) * 1000;

class MainMenu extends Component {
    componentDidMount() {
        M.AutoInit();
        const { onGetStores, onValidateToken, history } = this.props;
        this.clockToValidateToken = setInterval(() => onValidateToken(history), TIME);
        onValidateToken(history);
        onGetStores();
    }

    componentWillUnmount() {
        clearInterval(this.clockToValidateToken);
        const dropElement = document.getElementById('dropdown1');
        const collElement = document.getElementById('collapsible-id');
        if (collElement) {
            const collInstance = M.Collapsible.getInstance(collElement);
            if (collInstance) collInstance.destroy();
        }
        if (dropElement) {
            const dropInstance = M.Dropdown.getInstance(dropElement);
            if (dropInstance) dropInstance.destroy();
        }
    }

    render() {
        const { products } = this.props;
        const categories = utils.getCategories(products);
        return (
            <React.Fragment>
                <NavHeader />
                <div className="parrot-container">
                    <div className="row">
                        <div className="col s12">
                            <Categories
                                products={products}
                                categories={categories}
                            />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

MainMenu.propTypes = {
    onGetStores: PropTypes.func,
    onValidateToken: PropTypes.func,
    products: PropTypes.arrayOf(PropTypes.object),
    history: PropTypes.objectOf(PropTypes.any),
};

const mapStateToProps = state => ({
    currentStore: state.menu.currentStore,
    products: state.menu.products,
});

const mapDispatchToProps = dispatch => ({
    onGetStores: () => dispatch(actions.getStores()),
    onValidateToken: history => dispatch(authActions.validateToken(history)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainMenu));
