import React from 'react';
import PropTypes from 'prop-types';

const Product = ({ product, onUpdate }) => (
    <div className="card">
        <div className="row row-product">
            <div className="col s12 m4">
                <img src={product.imageUrl} alt={product.name} />
            </div>
            <div className="col s6 m4 product-price-description">
                <p>
                    {product.name}
                </p>
                <p>
                    $
                    {product.price || '0'}
                </p>
            </div>
            <div className="col s6 m4">
                <p>
                    Available
                </p>
                <div className="switch">
                    <label>
                        <input
                            id={product.uuid}
                            checked={product.availability === 'AVAILABLE'}
                            type="checkbox"
                            onChange={(e) => {
                                onUpdate(product.uuid, e.target.checked);
                            }}
                        />
                        <span className="lever" />
                    </label>
                </div>
            </div>
        </div>

    </div>
);

Product.propTypes = {
    product: PropTypes.objectOf(PropTypes.any),
    onUpdate: PropTypes.func,
};

export default Product;
