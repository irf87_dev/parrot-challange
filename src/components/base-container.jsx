import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Interceptor from '../utils/interceptor';

class Base extends Component {
    constructor() {
        super();
        this.interceptor = new Interceptor();
    }

    componentDidMount() {
        const { user } = this.props;
        if (user && user.access) {
            this.interceptor.access = user.access;
            this.interceptor.refresh = user.refresh;
        }
    }

    componentDidUpdate(prevProps) {
        const { user: oldUser } = prevProps;
        const { user } = this.props;

        if (oldUser.access !== user.access) {
            this.interceptor.access = user.access;
        }

        if (oldUser.refresh !== user.refresh) {
            this.interceptor.refresh = user.refresh;
        }
    }

    render() {
        const { children } = this.props;
        return (
            <React.Fragment>
                { children }
            </React.Fragment>
        );
    }
}

Base.propTypes = {
    children: PropTypes.node,
    user: PropTypes.objectOf(PropTypes.any),
};

const mapStateToProps = state => ({
    user: state.user,
});

export default withRouter(connect(mapStateToProps, { })(Base));
