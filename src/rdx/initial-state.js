export default {
    ui: {
        globalErrors: {
            showed: false,
            msg: '',
        },
        loader: {
            showed: false,
            msg: '',
        },
    },
    user: {
        username: '',
        access: '',
        refresh: '',
        email: '',
        firstName: '',
        lastName: '',
        uuid: '',
    },
    menu: {
        stores: [],
        products: [],
        currentStore: {},
    },
};
