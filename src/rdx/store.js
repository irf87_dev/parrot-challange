import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import reducers from './reducers/reducers';
import initialState from './initial-state';

const composeDebbug = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistConfig = {
    key: 'root',
    storage,
    timeout: null,
    whitelist: ['user'],
};

let localState = localStorage.getItem('persist:root');
if (localState) {
    localState = JSON.parse(localState);
    localState.user = JSON.parse(localState.user);
    delete localState._persist;
} else {
    localState = {};
}

const newInitialState = {
    ...initialState,
    ...localState,
};

const pReducer = persistReducer(persistConfig, reducers);
const store = createStore(pReducer, newInitialState, composeDebbug(applyMiddleware(thunk)));

const persistor = persistStore(store);

export { persistor, store };
