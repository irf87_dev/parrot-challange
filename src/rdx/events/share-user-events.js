export default {
    DO_LOGIN: 'DO_LOGIN',
    SET_USER_INFO: 'SET_USER_INFO',
    SET_TOKEN: 'SET_TOKEN',
    RESET_USER: 'RESET_USER',
};
