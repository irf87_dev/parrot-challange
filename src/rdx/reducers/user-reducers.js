import initialState from '../initial-state';
import EVENTS from '../events/share-user-events';

export default (state = initialState.user, action) => {
    switch (action.type) {
        case EVENTS.SET_USER_INFO:
            return {
                ...state,
                ...action.oUser,
            };
        case EVENTS.RESET_USER:
            return {
                user: initialState.user,
            };
        default:
            return { ...state };
    }
};
