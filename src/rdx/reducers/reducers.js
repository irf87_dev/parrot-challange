import { combineReducers } from 'redux';
import user from './user-reducers';
import ui from './ui-reducers';
import menu from './menu-reducers';

export default combineReducers({
    user,
    ui,
    menu,
});
