import initialState from '../initial-state';
import EVENTS from '../events/share-menu-events';

export default (state = initialState.menu, action) => {
    switch (action.type) {
        case EVENTS.SET_STORES:
            return {
                ...state,
                stores: action.stores,
            };
        case EVENTS.SET_PRODUCTS:
            return {
                ...state,
                products: action.products,
            };
        case EVENTS.SET_CURRENT_STORE:
            return {
                ...state,
                currentStore: action.store,
            };
        case EVENTS.UPDATE_PRODUCT:
            const { products } = state;
            const newArray = [...products];
            const index = products.findIndex(p => p.uuid === action.product.uuid);
            newArray[index] = action.product;
            return {
                ...state,
                products: newArray,
            };
        default:
            return { ...state };
    }
};
