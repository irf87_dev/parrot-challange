import initialState from '../initial-state';
import EVENTS from '../events/share-ui-events';

export default (state = initialState.ui, action) => {
    switch (action.type) {
        case EVENTS.CLOSE_LOADER:
            return {
                ...state,
                loader: {
                    showed: false,
                    msg: '',
                },
            };
        case EVENTS.SHOW_LOADER:
            return {
                ...state,
                loader: {
                    showed: true,
                    msg: action.msg,
                },
            };
        case EVENTS.SHOW_ERROR:
            return {
                ...state,
                globalErrors: {
                    showed: true,
                    msg: action.msg,
                },
            };
        case EVENTS.RESET_ERROR:
            return {
                ...state,
                globalErrors: state.globalErrors,
            };
        default:
            return { ...state };
    }
};
