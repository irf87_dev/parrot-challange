import EVENTS from '../events/share-ui-events';

const showLoader = (msg = '') => ({ type: EVENTS.SHOW_LOADER, msg });
const closeLoader = () => ({ type: EVENTS.CLOSE_LOADER });
const showError = (msg = '') => ({ type: EVENTS.SHOW_ERROR, msg });
const resetError = () => ({ type: EVENTS.RESET_ERROR });

export default {
    showLoader,
    closeLoader,
    showError,
    resetError,
};
