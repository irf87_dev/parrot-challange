import EVENTS from '../events/share-user-events';
import ApiHelper from '../../utils/api-helper';
import transformer from '../../utils/error-transformer';
import uiActions from './ui-action';

const {
    closeLoader,
    showLoader,
    showError,
    resetError,
} = uiActions;
const api = new ApiHelper();

const doLogin = (username, password, history) => (dispatch) => {
    dispatch(showLoader('Validando usuario'));
    dispatch(resetError());
    api.postJson('/api/auth/token', { username, password })
        .then((response) => {
            dispatch(closeLoader());
            const { access, refresh } = response;
            const oUser = {
                username,
                access,
                refresh,
            };
            dispatch({ type: EVENTS.SET_USER_INFO, oUser });
            setTimeout(() => history.push('/home'), 500);
        })
        .catch((error) => {
            dispatch(closeLoader());
            if (error.text) {
                error.text().then((errorMessage) => {
                    const errorMsg = transformer.errorTransformer(errorMessage);
                    dispatch(showError(errorMsg));
                });
            }
        });
};

const refreshToken = history => (dispatch, getState) => {
    const store = getState();
    const { user } = store;
    api.postJson('/api/auth/token/refresh', { refresh: user.refresh })
        .then((response) => {
            const { access, refresh } = response;
            const oUser = {
                access,
                refresh,
            };
            dispatch({ type: EVENTS.SET_USER_INFO, oUser });
            const isRootPath = window.location.pathname === '/';
            if (isRootPath) {
                setTimeout(() => history.push('/home'), 500);
            }
        })
        .catch((error) => {
            const { status } = error;
            if (status === 401) {
                window.location.replace('/');
            }
        });
};

const validateToken = history => (dispatch, getState) => {
    const store = getState();
    const { user } = store;
    const isRootPath = window.location.pathname === '/';
    if (!user.access) {
        if (!isRootPath) history.push('/');
        return;
    }
    api.get('/api/auth/token/test')
        .then((response) => {
            const { status } = response;
            const hasToRefresh = status !== 'ok'
                && user.refresh;
            const hasToRedirectHome = isRootPath
            && !hasToRefresh && user.access;

            if (hasToRedirectHome) {
                history.push('/home');
            } else if (hasToRefresh) {
                dispatch(refreshToken(history));
            }
        }).catch((error) => {
            const { status } = error;
            if (status === 401) {
                window.location.replace('/');
            }
        });
};

export default {
    doLogin,
    validateToken,
};
