import USER_EVENTS from '../events/share-user-events';
import MENU_EVENTS from '../events/share-menu-events';
import ApiHelper from '../../utils/api-helper';
import uiActions from './ui-action';

const { closeLoader, showLoader } = uiActions;
const api = new ApiHelper();

const setCurrentStore = store => ({ type: MENU_EVENTS.SET_CURRENT_STORE, store });

const updateProduct = product => ({ type: MENU_EVENTS.UPDATE_PRODUCT, product });

const getProducts = idStore => (dispatch) => {
    api.get(`/api/v1/products/?store=${idStore}`)
        .then((response) => {
            const { status, results } = response;
            dispatch(closeLoader());
            if (status === 'ok' && results && results.length) {
                dispatch({ type: MENU_EVENTS.SET_PRODUCTS, products: results });
            }
        }).catch((error) => {
            dispatch(closeLoader());
            const { status } = error;
            if (status === 401) {
                window.location.replace('/');
            }
        });
};

const getStores = () => (dispatch) => {
    dispatch(showLoader('Obteniendo datos'));
    api.get('/api/v1/users/me')
        .then((response) => {
            const { data } = response;
            if (data) {
                const { profile, stores } = data;
                const hasStores = stores && stores.length > 0;
                dispatch({ type: USER_EVENTS.SET_USER_INFO, oUser: profile });
                dispatch({ type: MENU_EVENTS.SET_STORES, stores });
                if (hasStores) {
                    dispatch(getProducts(stores[0].uuid));
                    dispatch(setCurrentStore(stores[0]));
                } else {
                    dispatch(closeLoader());
                }
            }
        }).catch((error) => {
            dispatch(closeLoader());
            const { status } = error;
            if (status === 401) {
                window.location.replace('/');
            }
        });
};

const updataAvailable = (productId, isAvailable = false) => (dispatch) => {
    dispatch(showLoader('Actualizando'));
    let availability = 'UNAVAILABLE';
    if (isAvailable) availability = 'AVAILABLE';
    const oParams = {
        availability,
        hint: '',
    };
    api.putJson(`/api/v1/products/${productId}/availability`, oParams)
        .then((response) => {
            dispatch(closeLoader());
            const { result, status } = response;
            if (status === 'ok' && result) {
                dispatch(updateProduct(result));
            }
        }).catch((error) => {
            dispatch(closeLoader());
            const { status } = error;
            if (status === 401) {
                window.location.replace('/');
            }
        });
};

const healthApi = () => () => {
    api.get('/api/health')
        .then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        });
};

export default {
    getStores,
    setCurrentStore,
    updataAvailable,
    healthApi,
};
