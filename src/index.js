import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import {
    Router,
    Route,
    Switch,
    BrowserRouter,
} from 'react-router-dom';
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import './index.css';
// General styles
import './styles/ui-styles.css';
import './styles/login-styles.css';
import './styles/products-styles.css';

import { Provider } from 'react-redux';
import { store } from './rdx/store';

// import App from './App';
import Login from './components/login/login';
import MainMenu from './components/menu/main';
import Loader from './components/share/loader';
import BaseContainer from './components/base-container';
// import registerServiceWorker from './registerServiceWorker';
// import reportWebVitals from './reportWebVitals';

const hist = createBrowserHistory();

const Root = () => (
    <Router history={hist}>
        <BrowserRouter>
            <Switch>
                <BaseContainer>
                    <Route exact path="/" component={Login} />
                    <Route exact path="/home" component={MainMenu} />
                </BaseContainer>
            </Switch>
            <Loader />
        </BrowserRouter>
    </Router>
);
ReactDOM.render(
    <Provider store={store}>
        <Root />
    </Provider>,
    document.getElementById('root'),
);
// registerServiceWorker();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
