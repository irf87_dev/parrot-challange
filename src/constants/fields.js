const LOGIN_FIELDS = {
    EMAIL: {
        label: 'INSERT_YOUR_EMAIL',
        onError: 'EMAIL_ERROR',
        validateType: 'email',
        fieldName: 'username',
    },
    PASSWORD: {
        label: 'INSERT_YOUR_PASSWORD',
        onError: 'PASSWORD_ERROR',
        validateType: 'require',
        fieldName: 'password',
    },
};

module.exports = {
    LOGIN_FIELDS,
};
