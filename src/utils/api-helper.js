class ApiHelper {
    constructor() {
        this.route = window.api;
    }

    /**
     * Hacer una peticion get
     * @param path
     * @param token
     * @returns {Promise<Response>}
     */
    get(path, token = false) {
        return fetch(this.route + path, {
            method: 'GET',
            headers: this.getHeaders(token),
        }).then((response) => {
            if (response.status !== 200) {
                throw response;
            }
            return response.json();
        }).catch(this.catchErrors);
    }

    /**
     * Hacer una peticion post
     * @param path
     * @param data
     * @param token
     * @returns {*}
     */
    post(path, data, token = false) {
        return this.makeFetch(path, data, token, 'POST');
    }

    postJson(path, data, token = false) {
        return this.makeFetchJson(path, data, token, 'POST');
    }

    /**
     * Hacer una peticion put
     * @param path
     * @param data
     * @param token
     * @returns {*}
     */
    put(path, data, token = false) {
        return this.makeFetch(path, data, token, 'PUT');
    }

    putJson(path, data, token = false) {
        return this.makeFetchJson(path, data, token, 'PUT');
    }

    delete(path, data, token = false) {
        return this.makeFetch(path, data, token, 'DELETE');
    }

    /**
     * Hacer una peticion
     * @param path
     * @param data
     * @param type
     * @param method
     * @returns {Promise<Response>}
     */
    makeFetch(path, data, token, method) {
        const form = new FormData();
        if (data) {
            Object.keys(data).forEach((key) => {
                form.append(key, data[key]);
            });
        }
        return fetch(this.route + path,
            {
                method,
                body: form,
                headers: this.getHeaders(token),
            }).then((response) => {
            if (response.status !== 200) {
                throw response;
            }
            return response.json();
        }).catch(this.catchErrors);
    }

    makeFetchJson(path, data, token, method) {
        const customHeaders = this.getHeadersJSON(token);

        return fetch(this.route + path,
            {
                method,
                body: JSON.stringify(data),
                headers: customHeaders,
            }).then((response) => {
            if (response.status !== 200) {
                throw response;
            }
            return response.json();
        }).catch(this.catchErrors);
    }

    /**
     * Headers generales que se usaran en el consumo de la api
     * @param token
     * @returns {{Accept: string}}
     */
    getHeaders(token) {
        const headers = {
            Accept: 'application/json',
            // 'Access-Control-Allow-Origin': '*',
            // 'Access-Control-Allow-Methods': '*',
        };
        if (token !== false) {
            headers.Authorization = `Bearer ${token}`;
        }

        return headers;
    }

    getHeadersJSON(token) {
        const headers = {
            // Accept: 'application/json',
            'Content-Type': 'application/json;charset=UTF-8',
            // 'Access-Control-Allow-Origin': '*',
            // 'Access-Control-Allow-Methods': '*',
        };
        if (token !== false) {
            headers.Authorization = `Bearer ${token}`;
        }

        return headers;
    }

    /**
     * Cachar errores cambiar el estado si hay errores
     * @param err
     */
    catchErrors(err) {
        // Cacha el body de las peticiones con error en el status
        throw err;
    }
}

export default ApiHelper;
