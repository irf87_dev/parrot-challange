const getCategories = (productsArray = []) => {
    let catagoriesArray = [];
    if (productsArray.length === 0) return catagoriesArray;
    const uniqueCategories = [];
    catagoriesArray = productsArray.map(product => product.category);
    try {
        for (let i = 0; i < catagoriesArray.length; i++) {
            const category = catagoriesArray[i];
            const { uuid } = category;
            const exists = uniqueCategories.find(item => item.uuid === uuid);
            if (!exists) {
                category.countingProducts = catagoriesArray.filter(item => item.uuid === uuid);
                category.countingProducts = category.countingProducts.length;
                uniqueCategories.push(category);
            }
        }
        uniqueCategories.sort((a, b) => ((a.sortPosition > b.sortPosition) ? 1 : -1));
    } catch (e) {
        console.error('Error on getCategories', e);
    }
    return uniqueCategories;
};

export default {
    getCategories,
};
