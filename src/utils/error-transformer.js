const errorTransformer = (errorMessage) => {
    const errorDefault = 'Error desconocido';
    try {
        const jsonError = JSON.parse(errorMessage);
        const { errors } = jsonError;
        const errorMsg = errors[0].message;
        switch (errorMsg) {
            case 'No active account found with the given credentials':
                return 'Usuario no activo';
            default:
                return errorDefault;
        }
    } catch (e) {
        console.error(e);
    }
    return errorDefault;
};

export default {
    errorTransformer,
};
