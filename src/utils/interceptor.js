/* eslint-disable */
import fetchIntercept from 'fetch-intercept';

class Interceptor {
    constructor() {
        this.exceptionsUrl = [
            '/api/auth/token/refresh',
            '/api/auth/token',
        ];
        this.unregister = fetchIntercept.register({
            request: (url, config) => {
                if (this.exceptionsUrl.indexOf(url) < 0) {
                    config.headers.Authorization = `Bearer ${this.access}`;
                }
                return [url, config];
            },
            requestError: (error) => {
                // Called when an error occured during another 'request' interceptor call
                return Promise.reject(error);
            },
            response: (response) => {
                // Modify the reponse object
                const { status } = response;
                if (status === 401) localStorage.removeItem('persist:root');
                return response;
            },
            responseError: (error) => {
                // Handle an fetch error
                return Promise.reject(error);
            },
        });
    }

    init() { console.log('init'); }
}

export default Interceptor;
